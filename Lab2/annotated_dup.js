var annotated_dup =
[
    [ "encoder", null, [
      [ "Encoder", "classencoder_1_1Encoder.html", "classencoder_1_1Encoder" ]
    ] ],
    [ "shares", null, [
      [ "Queue", "classshares_1_1Queue.html", "classshares_1_1Queue" ],
      [ "Share", "classshares_1_1Share.html", "classshares_1_1Share" ]
    ] ],
    [ "task_encoder", null, [
      [ "EncoderTasks", "classtask__encoder_1_1EncoderTasks.html", "classtask__encoder_1_1EncoderTasks" ]
    ] ],
    [ "task_user", null, [
      [ "UserTasks", "classtask__user_1_1UserTasks.html", "classtask__user_1_1UserTasks" ]
    ] ]
];