'''@file            main.py
   @brief           https://bitbucket.org/wimberle/me305_labs/src/master/305_Lab2/
   @details         lets task user and task encoder run together, provides clean UI

   @file            Lab2_Program_diagrams.pdf
                    click on link below to access and see source code
            
                    https://bitbucket.org/wimberle/me305_labs/src/master/305_Lab2/
            
                    https://bitbucket.org/wimberle/me305_labs/src/master/305_Lab2/program_diagrams.pdf


   @author:         Philip Pang
   @author:         Matthew Wimberley
   @mainpage
   @page            page1 Program Diagrams
                    
   @image           html   program_diagrams-1.png    "Task Diagram"
   @image           html   program_daigrams-2.png    "FSM Diagram"
   @include_path    D:\wimberle.bitbucket.io\Lab2\
'''

import task_encoder
import task_user
import shares



if __name__ == '__main__':

    zero_pos = shares.Share(0)
    curr_pos1 = shares.Share()
    delta1 = shares.Share()
    curr_pos2 = shares.Share()
    delta2 = shares.Share()
    delta_queue1 = shares.Queue()


    user = task_user.UserTasks(100, zero_pos, curr_pos1, delta1, curr_pos2, delta2, delta_queue1)
    updater = task_encoder.EncoderTasks(100, zero_pos, curr_pos1, delta1, curr_pos2, delta2, delta_queue1) # freq in Hz
 
''' @brief  user and updater instantiate all values we want from encoder
'''   
 
while(True):
        try:
            user.run()
            updater.run()
            
            '''@brief   runs both tasks simultaneously'''
            
        except KeyboardInterrupt:
            break

print('Program Terminating')


