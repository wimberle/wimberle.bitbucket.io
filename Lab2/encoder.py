""" @file       encoder.py
@brief      sets up encoder to be able to read values past 65535
@details    holds onto last value at each read, so negative values 
            and very large values can be stored and read with no hard limit 

@author:    Philip Pang
@author:    Matthew Wimberley
"""
import pyb

class Encoder:


    '''@brief       sets up class encoder to be used locally by other programs
       @details     saves last value of each cycle to be added or subtracted
       '''
       

    revolution = 0
    current_position = 0
    delta = 0
    
    '''@brief       zeroes encoder values'''

    def __init__(self, enc_number):
        
        '''@brief   sets up first encoder'''
        
        if enc_number == 1:
            self.timer = pyb.Timer(4, prescaler = 0, period = 65535)
            self.ch1 = self.timer.channel(1, pyb.Timer.ENC_AB, pin = pyb.Pin.cpu.B6)
            self.ch2 = self.timer.channel(2, pyb.Timer.ENC_AB, pin = pyb.Pin.cpu.B7)            
        else:
            '''@brief   sets up second encoder'''
        
            self.timer = pyb.Timer(3, prescaler = 0, period = 65535)
            self.ch1 = self.timer.channel(1, pyb.Timer.ENC_AB, pin = pyb.Pin.cpu.C6)
            self.ch2 = self.timer.channel(2, pyb.Timer.ENC_AB, pin = pyb.Pin.cpu.C7)  

    def update(self):

        
        '''@brief       needs at least 2 values in each period
           @details     if >=2 values then delta can be accurately recorded
                        saved and then subtracted from last known value '''
                        

        prev_position = self.current_position % 65535
        self.delta = self.timer.counter() - prev_position
        if self.delta < -65535/2:
            self.delta += 65535
        elif self.delta > 65535/2:
            self.delta -= 65535
        self.current_position += self.delta
        
        
    def get_position(self):
        
        '''@brief   retrieves current position
           @return  current position '''
        
        return self.current_position
    
    def set_position(self, position):

                
        '''@brief   sets current position
           @param   current position '''
           

        self.timer.counter(0)
        self.current_position = position

    def get_delta(self):

        '''@brief   retrieves delta value
           @param   self '''
           
        return self.delta