""" @file       task_encoder.py
@brief      creates objects of all variables in main in task_encoder
@details    sets up use for both encoders to relay informations
@author     Philip Pang
@author     Matthew Wimberley
"""
import utime
import encoder

class EncoderTasks:
    
    '''@brief instantiates self object of encoder tasks'''
    
       

           

    def __init__(self, freq, zero_pos, curr_pos1, delta1, curr_pos2, delta2, delta_queue1):
    
        '''@brief   initializes all encoder information
           @param   same as main.py '''
           
        self.zero_pos = zero_pos
        self.curr_pos1 = curr_pos1
        self.delta1 = delta1
        self.curr_pos2 = curr_pos2
        self.delta2 = delta2
        self.delta_queue = delta_queue1
        self.period = 1/freq * 1000000       # in us (microseconds)
        self.next_time = utime.ticks_us()+ self.period
        self.enc1 = encoder.Encoder(1)
        self.enc2 = encoder.Encoder(2)
    
    def run(self):
        if(utime.ticks_us()>=self.next_time):

            
             '''@brief   as long as time continues forward, position and encoder data is retrieved
                @param   must call object of itself'''
           
        if(self.zero_pos.read()):


                self.enc1.set_position(0)
                self.enc2.set_position(0)
                self.zero_pos.write(False)

#@brief   resets encoder positions then updates it

            
                self.enc1.update()
                self.enc2.update()

                self.delta1.write(self.enc1.get_delta())
                self.curr_pos1.write(self.enc1.get_position())
                self.delta2.write(self.enc2.get_delta())
                self.curr_pos2.write(self.enc2.get_position())
                self.next_time += self.period
                
              
            
    
            
            
