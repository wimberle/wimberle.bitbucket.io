var annotated_dup =
[
    [ "DRV8847", null, [
      [ "DRV8847", "classDRV8847_1_1DRV8847.html", "classDRV8847_1_1DRV8847" ],
      [ "Motor", "classDRV8847_1_1Motor.html", "classDRV8847_1_1Motor" ]
    ] ],
    [ "encoder", null, [
      [ "Encoder", "classencoder_1_1Encoder.html", "classencoder_1_1Encoder" ]
    ] ],
    [ "shares", null, [
      [ "Queue", "classshares_1_1Queue.html", "classshares_1_1Queue" ],
      [ "Share", "classshares_1_1Share.html", "classshares_1_1Share" ]
    ] ],
    [ "task_encoder", null, [
      [ "EncoderTasks", "classtask__encoder_1_1EncoderTasks.html", "classtask__encoder_1_1EncoderTasks" ]
    ] ],
    [ "task_motor", null, [
      [ "MotorTasks", "classtask__motor_1_1MotorTasks.html", "classtask__motor_1_1MotorTasks" ]
    ] ],
    [ "task_safety", null, [
      [ "SafetyTasks", "classtask__safety_1_1SafetyTasks.html", "classtask__safety_1_1SafetyTasks" ]
    ] ],
    [ "task_user", null, [
      [ "UserTasks", "classtask__user_1_1UserTasks.html", "classtask__user_1_1UserTasks" ]
    ] ]
];