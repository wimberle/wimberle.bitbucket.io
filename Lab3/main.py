''' @file           main.py
    @brief          instantiates variables and runs tasks simultaneously
    @details        lets task user and task encoder run together, provides clean UI
                    click on link below to access and see source code
            
                    https://bitbucket.org/wimberle/me305_labs/src/master/305_Lab3/main.py
            


    @author         Philip Pang
    @author         Matthew Wimberley
    @date           November 6, 2021
'''

import task_encoder
import task_user
import task_motor
import task_safety
import DRV8847
import pyb
import shares
import encoder
import ClosedLoop
import task_controller


if __name__ == '__main__':
    
    ''' @brief      user and updater instantiate all values we want from encoder
    '''


    zero_pos1 = shares.Share(0)
    zero_pos2 = shares.Share(0)
    curr_pos1 = shares.Share()
    delta1 = shares.Share()
    curr_pos2 = shares.Share()
    delta2 = shares.Share()
    duty1 = shares.Share(0)
    duty2 = shares.Share(0)
    collect1 = shares.Share(0)
    collect2 = shares.Share(0)
    stop1 = shares.Share(0)
    stop2 = shares.Share(0)
    data_queue1 = shares.Queue()
    data_queue2 = shares.Queue()
    startTime1 = shares.Share(0)
    startTime2 = shares.Share(0)
    fault_flag = shares.Share(0)
    clr_fault = shares.Share(1)
    enable_motors = shares.Share(1)

    step1 = shares.Share(0)
    step2 = shares.Share(0)
    alg1 = shares.Share(1)
    alg2 = shares.Share(1)
    kp1 = shares.Share(0)
    kp2 = shares.Share(0)
    ki1 = shares.Share(0)
    ki2 = shares.Share(0)
    kd1 = shares.Share(0)
    kd2 = shares.Share(0)

    freq = 10 # Hz

    DRV8847 = DRV8847.DRV8847(3, pyb.Pin(pyb.Pin.cpu.A15), pyb.Pin(pyb.Pin.cpu.B2), fault_flag)
    motor1 = DRV8847.motor(1)
    motor2 = DRV8847.motor(2)
    enc1 = encoder.Encoder(4, pyb.Pin(pyb.Pin.cpu.B6), pyb.Pin(pyb.Pin.cpu.B7))
    enc2 = encoder.Encoder(8, pyb.Pin(pyb.Pin.cpu.C6), pyb.Pin(pyb.Pin.cpu.C7))
    loop1 = ClosedLoop.ClosedLoop(100, -100)
    loop2 = ClosedLoop.ClosedLoop(100, -100)

    user = task_user.UserTasks(freq, zero_pos1, zero_pos2, curr_pos1, delta1, data_queue1, curr_pos2, delta2, data_queue2,
                               duty1, duty2, collect1, collect2, stop1, stop2, startTime1, startTime2, fault_flag,
                               clr_fault, enable_motors, step1, step2, alg1, alg2, kp1, kp2, ki1, ki2, kd1, kd2)
    read_boi1 = task_encoder.EncoderTasks(enc1, freq, zero_pos1, curr_pos1, delta1, duty1, collect1, stop1, data_queue1, startTime1) # freq in Hz
    read_boi2 = task_encoder.EncoderTasks(enc2, freq, zero_pos2, curr_pos2, delta2, duty2, collect2, stop2, data_queue2, startTime2)
    spin_boi1 = task_motor.MotorTasks(freq, motor1, duty1, fault_flag)
    spin_boi2 = task_motor.MotorTasks(freq, motor2, duty2, fault_flag)
    faultty = task_safety.SafetyTasks(freq, DRV8847, fault_flag, clr_fault, enable_motors)

    controller1 = task_controller.ControllerTasks(freq, step1, loop1, duty1, delta1, alg1, startTime1, stop1, kp1, ki1, kd1)
    controller2 = task_controller.ControllerTasks(freq, step2, loop2, duty2, delta2, alg2, startTime2, stop2, kp2, ki2, kd2)


    while(True):
            try:
                '''@brief   runs all tasks simultaneously
                   @details reading, fault flags, controller, and spinning all ran here
                   '''

                user.run()
                read_boi1.run()
                read_boi2.run()
                spin_boi1.run()
                spin_boi2.run()
                faultty.run()

                controller1.run()
                controller2.run()

            except KeyboardInterrupt:
                break

    print('Program Terminating')


