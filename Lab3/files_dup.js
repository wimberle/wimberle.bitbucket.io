var files_dup =
[
    [ "DRV8847.py", "DRV8847_8py.html", "DRV8847_8py" ],
    [ "encoder.py", "encoder_8py.html", [
      [ "encoder.Encoder", "classencoder_1_1Encoder.html", "classencoder_1_1Encoder" ]
    ] ],
    [ "main.py", "main_8py.html", null ],
    [ "shares.py", "shares_8py.html", [
      [ "shares.Share", "classshares_1_1Share.html", "classshares_1_1Share" ],
      [ "shares.Queue", "classshares_1_1Queue.html", "classshares_1_1Queue" ]
    ] ],
    [ "task_encoder.py", "task__encoder_8py.html", [
      [ "task_encoder.EncoderTasks", "classtask__encoder_1_1EncoderTasks.html", "classtask__encoder_1_1EncoderTasks" ]
    ] ],
    [ "task_motor.py", "task__motor_8py.html", [
      [ "task_motor.MotorTasks", "classtask__motor_1_1MotorTasks.html", "classtask__motor_1_1MotorTasks" ]
    ] ],
    [ "task_safety.py", "task__safety_8py.html", [
      [ "task_safety.SafetyTasks", "classtask__safety_1_1SafetyTasks.html", "classtask__safety_1_1SafetyTasks" ]
    ] ],
    [ "task_user.py", "task__user_8py.html", [
      [ "task_user.UserTasks", "classtask__user_1_1UserTasks.html", "classtask__user_1_1UserTasks" ]
    ] ]
];