var classBNO055_1_1BNO055 =
[
    [ "__init__", "classBNO055_1_1BNO055.html#a9d0f1bc8db9bbcc124e3992466c3210e", null ],
    [ "calib_status", "classBNO055_1_1BNO055.html#aca9a22e2d1ca740e7a25d4346a9d4fad", null ],
    [ "calibrate", "classBNO055_1_1BNO055.html#aac7e7af1ab189be6d48906a063fa249d", null ],
    [ "change_mode", "classBNO055_1_1BNO055.html#a46879e92a286997c935930164e6d850d", null ],
    [ "print_calib", "classBNO055_1_1BNO055.html#a5935bc4c0a7fccb090e3a256279c5434", null ],
    [ "read_angles", "classBNO055_1_1BNO055.html#a8e524be120b05e6c50bc92fc653cdc33", null ],
    [ "read_coeff", "classBNO055_1_1BNO055.html#afb2695a76af2ea037cceb7f935f95dda", null ],
    [ "read_velocity", "classBNO055_1_1BNO055.html#af2d0ade6143a8a39dee781271f24da15", null ],
    [ "set_units", "classBNO055_1_1BNO055.html#a7551a5b702b66b121bde625ff581700d", null ],
    [ "write_coeff", "classBNO055_1_1BNO055.html#a5110781f8ff3768a5b1ce6163c4e0280", null ]
];