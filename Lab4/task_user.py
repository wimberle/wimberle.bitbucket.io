'''@file            task_user.py
   @brief           communicates user input with nucleo board
   @details         does not need to import other files but will be instead called through main
   @details         different inputs will run different commands
                    click below to find source code:
                    https://bitbucket.org/wimberle/me305_labs/src/master/305_Lab3/task_user.py

   @author          Philip Pang
   @author          Matthew Wimberley
'''
import pyb
import utime
import array


class UserTasks:
    '''@brief       discerns user input
       @details     all UI focused here'''

    def __init__(self, freq, zero_pos1, zero_pos2, curr_pos1, delta1, data_queue1, curr_pos2, delta2, data_queue2,
                 duty1, duty2, collect1, collect2, stop1, stop2, currentTime, startTime1, startTime2, fault_flag, clr_fault,
                 enable_motors, step1, step2, alg1, alg2, kp1, kp2, ki1, ki2, kd1, kd2, target1, target2):
        '''@brief   initialized variables just like in task_encoder and main
           @param   all positional values related to both encoders
           '''

        self.zero_pos1 = zero_pos1
        self.zero_pos2 = zero_pos2
        self.curr_pos1 = curr_pos1
        self.delta1 = delta1
        self.curr_pos2 = curr_pos2
        self.delta2 = delta2
        self.data_queue1 = data_queue1
        self.data_queue2 = data_queue2
        self.duty1 = duty1
        self.duty2 = duty2
        self.collect1 = collect1
        self.collect2 = collect2
        self.stop1 = stop1
        self.stop2 = stop2
        self.fault_flag = fault_flag
        self.clr_fault = clr_fault
        self.enable_motors = enable_motors
        self.startTime1 = startTime1
        self.startTime2 = startTime2

        self.step1 = step1
        self.step2 = step2
        self.alg1 = alg1
        self.alg2 = alg2
        self.kp1 = kp1
        self.kp2 = kp2
        self.ki1 = ki1
        self.ki2 = ki2
        self.kd1 = kd1
        self.kd2 = kd2
        self.target1 = target1
        self.target2 = target2

        self.period = 1 / freq * 1000000 # in us (microseconds)
        self.freq = freq
        self.port = pyb.USB_VCP()
        self.state = 0
        self.currentTime = utime.ticks_us()
        self.next_time = 0
        
        self.time_array1 = array.array('d',[])
        self.velocity_array1 = array.array('d',[])
        self.duty_array1 = array.array('d',[])
        
        self.time_array2 = array.array('d',[])
        self.velocity_array2 = array.array('d',[])
        self.duty_array2 = array.array('d',[])

    def print_data(self, enc):

        '''@brief   retrieves and prints queue
           @return  returns 0 value
           '''
           
        print("\nTime (s):   Velocity (rad/s):   Effort-PWM (%):")
        if enc == 1:
            for i in range(0, len(self.time_array1)):
                print("{0:<12}, {1:<16}, {2:<20}".format(round(self.time_array1[i] / 1000000, 3),
                                                         round(self.velocity_array1[i] * 2.0 * 3.1415926535 / 4000 * self.freq, 2),
                                                         round(self.duty_array1[i])))
        elif enc == 2:
            for i in range(0, len(self.time_array2)):
                print("{0:<12}, {1:<16}, {2:<20}".format(round(self.time_array2[i] / 1000000, 3),
                                                         round(self.velocity_array2[i] * 2.0 * 3.1415926535 / 4000 * self.freq, 2),
                                                         round(self.duty_array2[i])))
        print("----------------------------DATA COLLECTION DONE----------------------------\n")
        

    def read_duty(self):
        
        '''@brief   reads duty cycle input by user
           @details accounts for user entering data as well as nonsensical answers
           @param   duty cycle
           '''
        
        duty = ""
        place = 0
        user_in = ""
        while (user_in != "\r" and user_in != "\n"):
            if (self.port.any()):
                user_in = self.port.read(1).decode()

                if (user_in.isdigit()):
                    duty += user_in
                    self.port.write(user_in)
                    place += 1
                elif (user_in == "-"):
                    if (place == 0):
                        duty += user_in
                        self.port.write(user_in)
                        place += 1
                    else:
                        continue
                elif (user_in == "."):
                    if ("." in duty):
                        continue
                    else:
                        duty += user_in
                        self.port.write(user_in)
                        place += 1
                elif (user_in == "\x7F"):
                    if (place == 0):
                        continue
                    else:
                        duty = duty[:-1]
                        self.port.write('\x7F')
                        place -= 1

        return float(duty)

    def clear_arrays(self, enc):
        if enc == 1:
            self.time_array1 = array.array('d',[])
            self.velocity_array1 = array.array('d',[])
            self.duty_array1 = array.array('d',[])
        else:
            self.time_array2 = array.array('d',[])
            self.velocity_array2 = array.array('d',[])
            self.duty_array2 = array.array('d',[])

    def run(self):

        '''@brief   runs task user
           @details constantly waiting for correct input from user to execute
       '''

        if (utime.ticks_us() >= self.next_time):

            self.next_time = utime.ticks_us() + self.period

            '''@brief   if encoder time still running the rest will execute
            '''

            if (self.state == 0):
                '''@brief   state 0 gives instructions then proceeds to state 1
                '''

                print("+---------------------------------------------------------------------------+\n"
                      "| Step Response Testing Interface - Matt/Philip                             |\n"
                      "+---------------------------------------------------------------------------+\n"
                      "| Use the following commands:                                               |\n"
                      "|   z       Zero the position of encoder 1                                  |\n"
                      "|   Z       Zero the position of encoder 2                                  |\n"
                      "|   p       Print out the position of encoder 1                             |\n"
                      "|   P       Print out the position of encoder 2                             |\n"
                      "|   d       Print out the delta for encoder 1                               |\n"
                      "|   D       Print out the delta for encoder 2                               |\n"
                      "|   m       Enter a duty cycle for motor 1                                  |\n"
                      "|   M       Enter a duty cycle for motor 2                                  |\n"
                      "| c or C    Clear a fault condition triggered by the DRV8847                |\n"
                      "|   g       Collect data from encoder 1 for 30 seconds                      |\n"
                      "|   G       Collect data from encoder 2 for 30 seconds                      |\n"
                      "|   s       End data collection from \"g\" command immediately                |\n"
                      "|   S       End data collection from \"G\" command immediately                |\n"
                      "|   1       Perform a step response on motor 1                              |\n"
                      "|   2       Perform a step response on motor 2                              |\n"
                      "|   h       Display this help message                                       |\n"                      
                      "| Ctrl + C  End program                                                     |\n"
                      "+---------------------------------------------------------------------------+\n"
                      )

                self.state = 1

            elif (self.state == 1):

                '''@brief   waits for 6 commands listed above
                   @param   one of 6 inputs
                   @details if any of 6 commands is hit then appropriate command will follow
                '''
                
                if self.stop1.read():
                    self.collect1.write(0)
                    self.step1.write(0)
                    self.stop1.write(0)
                    self.print_data(1)

                elif self.collect1.read():

                    while self.data_queue1.num_in() > 0:
                        tuplee = self.data_queue1.get()
                        self.time_array1.append(tuplee[0])
                        self.velocity_array1.append(tuplee[1])
                        self.duty_array1.append(tuplee[2])

                    '''self.currentTime1 = utime.ticks_us()
                    if len(self.time_array1) == 0:
                        self.time_array1.append(0)
                        self.velocity_array1.append(self.delta1.read())
                        self.duty_array1.append(self.duty1.read())
                         
                        self.startTime1 = utime.ticks_us()
                        self.next_time1 = utime.ticks_us() + self.period
                        
                    #print(self.currentTime)
                    if utime.ticks_diff(self.currentTime1, self.next_time1) >= 0:
                        self.time_array1.append(self.currentTime1 - self.startTime1)
                        self.velocity_array1.append(self.delta1.read())
                        self.duty_array1.append(self.duty1.read())
                        self.next_time1 = utime.ticks_us() + self.period
            
                    if self.currentTime1 - self.startTime1 >= 30000000:
                        self.print_data(1)
                        self.collect1.write(0)
                        del self.time_array1
                        self.time_array1 = array.array('d', [])
                        del self.velocity_array1
                        self.velocity_array1 = array.array('d', [])
                        del self.duty_array1
                        self.duty_array1 = array.array('d', [])'''


                if self.stop2.read():
                    self.collect2.write(0)
                    self.step2.write(0)
                    self.stop2.write(0)
                    self.print_data(2)

                elif self.collect2.read():
                    while self.data_queue2.num_in() > 0:
                        tuplee = self.data_queue2.get()
                        self.time_array2.append(tuplee[0])
                        self.velocity_array2.append(tuplee[1])
                        self.duty_array2.append(tuplee[2])

                if (self.fault_flag.read()):
                    print("A FAULT has occurred | Press \'c\' or \'C\' to clear fault AND print data...")
                    self.collect1.write(0)
                    self.collect2.write(0)
                    self.state = 2
                # waiting for user input state

                elif (self.port.any()):

                    user_input = self.port.read(1).decode()

                    if user_input == ('z'):
                        print("Zeroing Encoder 1\n")
                        self.zero_pos1.write(True)

                    elif user_input == ('Z'):
                        print("Zeroing Encoder 2\n")
                        self.zero_pos2.write(True)

                    elif user_input == ('p'):
                        print("E1: t = {:}, p = {:}\n".format(utime.ticks_ms(), self.curr_pos1.read()))

                    elif user_input == ('P'):
                        print("E2: t = {:}, p = {:}\n".format(utime.ticks_ms(), self.curr_pos2.read()))

                    elif user_input == ('d'):
                        print("E1: t = {:}, d = {:}\n".format(utime.ticks_ms(), self.delta1.read()))

                    elif user_input == ('D'):
                        print("E2: t = {:}, d = {:}\n".format(utime.ticks_ms(), self.delta2.read()))

                    elif user_input == ('m'):
                        print("Enter a duty cycle for Motor 1:")
                        duty_cycle = self.read_duty()
                        print("\nMotor 1 duty cycle set to: " + str(duty_cycle) + "\n")
                        self.duty1.write(int(duty_cycle))

                    elif user_input == ('M'):
                        print("Enter a duty cycle for Motor 2:")
                        duty_cycle = self.read_duty()
                        print("\nMotor 2 duty cycle set to: " + str(duty_cycle) + "\n")
                        self.duty2.write(int(duty_cycle))

                    elif user_input == ('1'):
                        alg = 1
                        print("Enter Kp gain for P-control on motor 1 [PWM%/(rad/s)]: ")
                        kp = self.read_duty()
                        print("\nKp has been set to: " + str(kp) + "\n")
                        print("Enter velocity set-point [rad/s]: ")
                        self.target1.write(self.read_duty())
                        print("\nVelocity set-point has been set to: " + str(self.target1.read()) + "\n")
                        print("Running step response \n")
                        self.alg1.write(alg)
                        self.kp1.write(kp)
                        while self.data_queue1.num_in():
                            self.data_queue1.get()
                        self.clear_arrays(1)
                        self.startTime1.write(utime.ticks_us())
                        self.step1.write(1)
                        self.collect1.write(1)


                    elif user_input == ('2'):
                        alg = 1
                        print("Enter Kp gain for P-control on motor 2: ")
                        kp = self.read_duty()
                        print("\nKp has been set to: " + str(kp) + "\n")
                        print("Enter velocity set-point [rad/s]: ")
                        self.target2.write(self.read_duty())
                        print("\nVelocity set-point has been set to: " + str(self.target2.read()) + "\n")
                        print("Running step response \n")
                        self.alg2.write(alg)
                        self.kp2.write(kp)
                        while self.data_queue2.num_in():
                            self.data_queue2.get()
                        self.clear_arrays(2)
                        self.startTime2.write(utime.ticks_us())
                        self.step2.write(1)
                        self.collect2.write(1)


                    elif self.collect1.read() == 0 and user_input == ('g'):
                        print("------------------------ START MOTOR #1 DATA COLLECTION ------------------------\n")
                        self.collect1.write(1)
                        while self.data_queue1.num_in():
                            self.data_queue1.get()
                        self.clear_arrays(1)
                        self.startTime1.write(utime.ticks_us())
                        

                    elif self.collect2.read() == 0 and user_input == ('G'):
                        print("------------------------ START MOTOR #2 DATA COLLECTION ------------------------\n")
                        self.collect2.write(1)
                        while self.data_queue2.num_in():
                            self.data_queue2.get()
                        self.clear_arrays(2)
                        self.startTime2.write(utime.ticks_us())


                    elif self.collect1.read() == 1 and user_input == ('s'):
                        self.collect1.write(0)
                        self.step1.write(0)
                        self.stop1.write(0)
                        self.print_data(1)

                    elif self.collect2.read() and user_input == ('S'):
                        self.collect2.write(0)
                        self.step2.write(0)
                        self.stop2.write(0)
                        self.print_data(2)

                    elif user_input == ('h'):
                        self.state = 0

            elif (self.state == 2):
                if (self.port.any()):
                    user_input = self.port.read(1).decode()
                    if user_input == ('c') or user_input == ('C'):
                        print("FAULT CLEARED\n")
                        self.clr_fault.write(1)
                        self.state = 1
                        self.step1.write(0)
                        self.step2.write(0)
                        self.duty1.write(0)
                        self.duty2.write(0)
                        self.stop1.write(1)
                        self.stop2.write(1)
                        

