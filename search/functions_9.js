var searchData=
[
  ['scan_5fall_0',['scan_all',['../classTouch__Panel_1_1Touch__Panel.html#a45b017007b316edb0836e5532022dc61',1,'Touch_Panel::Touch_Panel']]],
  ['set_5fduty_1',['set_duty',['../classDRV8847_1_1Motor.html#ae2e6c0feeb46de3f93c35e7f25a79a8b',1,'DRV8847::Motor']]],
  ['set_5fkd_2',['set_Kd',['../classClosedLoop_1_1ClosedLoop.html#a971535934864169982edcef715af5093',1,'ClosedLoop::ClosedLoop']]],
  ['set_5fki_3',['set_Ki',['../classClosedLoop_1_1ClosedLoop.html#a24cfb9cbbbe0ed5afeb7f4d8e130d59d',1,'ClosedLoop::ClosedLoop']]],
  ['set_5fkp_4',['set_Kp',['../classClosedLoop_1_1ClosedLoop.html#aa6b1cd603a27ba90b9e3b4dda66d9591',1,'ClosedLoop::ClosedLoop']]],
  ['set_5fposition_5',['set_position',['../classencoder_1_1Encoder.html#a097746ac59abf28e6567f5604fe83c1f',1,'encoder::Encoder']]]
];
