'''@file                mainpage.py
   @brief               Brief doc for mainpage.py
   @details             Detailed doc for mainpage.py 

   @mainpage

   @section sec_intro   Introduction
                        Displays 3 LED patterns

   @section sec_mot     Motor Driver
                        Some information about the motor driver with links.
                        Please see motor.Motor for details.

   @section sec_enc     Encoder Driver
                        Some information about the encoder driver with links. 
                        Please see encoder.Encoder for details.

   @author              Matthew Wimberley

   @copyright           License Info

   @date                10/19/2021
'''