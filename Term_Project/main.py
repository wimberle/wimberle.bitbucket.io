''' @file           main.py
    @brief          instantiates objects and runs tasks simultaneously
    @details        This file calls all other tasks to be run cooperatively for
                    our ball balancing machine. The most recent version of a 
                    main file can be found here:           
                    https://bitbucket.org/wimberle/me305_labs/src/master/305_Term_Project/main.py


    @author         Philip Pang
    @author         Matthew Wimberley
    @date           December 5, 2021
'''
from pyb import Pin, ADC, I2C
from ulab import numpy as np
import shares

import BNO055
import DRV8847
import Touch_Panel

import task_controller      # take care of case when ball falls off board
import task_touch
import task_imu
import task_user
# import task_motor         # -----ONLY USED TO HANDLE FAULTS

if __name__ == '__main__':
    
    ''' @brief      instantiates all objects we want for our board
        @details    sets up shares for IMU calibration data and shares for data
                    collection 
    '''

    touch_state = shares.Share(())              # (x, y, x_dot, y_dot)
    imu_state = shares.Share(())                # (theta_y [roll], theta_x [pitch], omega_y, omega_x)


    balancing = shares.Share(0)
    collect = shares.Share(0)
    stop_data = shares.Share(0)
    startTime = shares.Share(0)
    '''@brief       all data collection in Shares file
       @details     One array cannot hold all the collection data, so we must use
                    shares and queues to hold some of the collection data as the 
                    controller reacts faster than the collection.
    '''

    touch_queue = shares.Queue()
    imu_queue = shares.Queue()
    time_duty_queue = shares.Queue()

    # -----DO NOT DELETE ------------------------------
    # control inputs: state (share), K_gains, balancing (share), collect (share), stop_data (share)
    # control action: set duty, interface with motor, collect data!!
    # control outputs: time elapsed + state + duty (data to collect)
    #                       - queues: Touch, IMU, Time + Duty

    # touch inputs: balancing (share)
    # touch action: store last position, calculate velocity
    # touch outputs: state (position)(share)

    # imu inputs: balancing (share)
    # imu action: store last angular position, calculate angular velocity
    # imu outputs: state (angular)(share)

    freq_io = 200    # touch panel, controller, motor,
    freq_user = 50      # user task
    freq_imu = 100   # IMU task (limited)
    '''@brief       establishes user and imu frequency to be handled by controller
    '''

    Touch_Panel = Touch_Panel.Touch_Panel(Pin.cpu.A7, Pin.cpu.A1, Pin.cpu.A6, Pin.cpu.A0)
    '''@brief       instantiates touch panel
    '''
    
    Touch_Panel.calibrate()

    BNO055 = BNO055.BNO055(I2C(1, I2C.MASTER))
    '''@brief       instantiates inertial measurement unit
    '''
    
    BNO055.calibrate()

    Motor1 = DRV8847.Motor(3, 1, 2, Pin(Pin.cpu.B4), Pin(Pin.cpu.B5))
    '''@brief       instantiates pin motor object; this motor is for theta_y
    '''
    
    Motor2 = DRV8847.Motor(3, 3, 4, Pin(Pin.cpu.B0), Pin(Pin.cpu.B1))
    '''@brief       instantiates pin motor object; this motor is for theta_x
    '''  

    user = task_user.UserTasks(freq_user, balancing, collect, stop_data, startTime, touch_queue, imu_queue, time_duty_queue)
    '''@brief       instantiates user object for all the collection queues
    '''

    controller = task_controller.ControllerTasks(freq_io, Motor1, Motor2, touch_state, imu_state, balancing, collect, stop_data,
                                                  startTime, touch_queue, imu_queue, time_duty_queue)
    '''@brief       instantiates controller object for all the collection queues and motor input
    '''

    panel = task_touch.TouchTasks(freq_io, Touch_Panel, balancing, collect, touch_state)
    '''@brief       instantiates touch panel object
    '''
    
    imu = task_imu.IMUTasks(freq_imu, BNO055, balancing, collect, imu_state)
    '''@brief       instantiates inertial measurement unit 
    '''

    while True:
            try:
                '''@brief   runs all tasks simultaneously
                   @details We initially had the controller task run before the
                            panel, but this was too slow and the data collection
                            was working too slow behind the touch panel, so we have
                            the panel running first. Panel, IMU, controller, then 
                            user interface seems to be the most cooperative order
                            for us. 
                 
                   '''

                

                panel.run()
                imu.run()
                
                controller.run()
                
                user.run()
                
                
            except KeyboardInterrupt:
                break

    print('Program Terminating')
