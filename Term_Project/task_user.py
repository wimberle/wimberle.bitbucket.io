'''@file            task_user.py
   @brief           communicates user input with nucleo board
   @details         does not need to import other files but will be instead called through main
   @details         different inputs will run different commands
                    click below to find source code for Lab 3:
                    https://bitbucket.org/wimberle/me305_labs/src/master/305_Lab3/task_user.py
                    Click below to find our most recent task_user source code:
                    https://bitbucket.org/wimberle/me305_labs/src/master/305_Term_Project/task_user.py

   @author          Philip Pang
   @author          Matthew Wimberley
   @date            November 10, 2021
'''
import pyb
import utime
import array


class UserTasks:
    '''@brief       discerns user input
       @details     all UI focused here'''

    def __init__(self, freq_user, balancing, collect, stop_data, startTime, touch_queue, imu_queue, time_duty_queue):
        '''@brief   passes ball balancing and collection through queues and shares
           @param   frequency at which controller takes in user input
           @param   balancing object
           @param   collection object
           @param   Stop and Start times
           @param   touch panel and inertial measurement unit queues
           @param   duty time queue
           '''

        self.touch_queue = touch_queue
        self.imu_queue = imu_queue
        self.time_duty_queue = time_duty_queue
        self.collect = collect
        self.stop_data = stop_data
        self.balancing = balancing
        self.startTime = startTime

        self.period = 1 / freq_user * 1000000  # in us (microseconds)
        self.freq_user = freq_user
        self.port = pyb.USB_VCP()
        self.state = 0
        self.currentTime = utime.ticks_us()
        self.next_time = 0

        self.time_array = array.array('d', [])
        self.X_array = array.array('d', [])
        self.Y_array = array.array('d', [])
        self.X_vel_array = array.array('d', [])
        self.Y_vel_array = array.array('d', [])
        self.Theta_x_array = array.array('d', [])
        self.Theta_y_array = array.array('d', [])
        self.Omega_x_array = array.array('d', [])
        self.Omega_y_array = array.array('d', [])
        self.PWM1_array = array.array('i', [])
        self.PWM2_array = array.array('i', [])

    def print_data(self):
        '''@brief   prints data from arrays
           @return  returns null
           '''

        print(
            "\nTime (s):,   X (mm):,  Y (mm):, X_vel (mm/s):, Y_vel (mm/s):, Theta_x (rad):, Theta_y (rad):, Omega_x (rad/s):, Omega_y (rad/s):, PWM_1 (%):, PWM_2 (%):")
        for i in range(0, len(self.time_array)):
            print("{:<12}, {:<12}, {:<12}, {:<12}, {:<12}, {:<12}, {:<12}, {:<12}, {:<12}, {:<12}, {:<12}"
                  .format(round(self.time_array[i] / 1000000, 3),
                          round(self.X_array[i], 2),
                          round(self.Y_array[i], 2),
                          round(self.X_vel_array[i], 2),
                          round(self.Y_vel_array[i], 2),
                          round(self.Theta_x_array[i], 2),
                          round(self.Theta_y_array[i], 2),
                          round(self.Omega_x_array[i], 2),
                          round(self.Omega_y_array[i], 2),
                          round(self.PWM1_array[i]),
                          round(self.PWM2_array[i])))
        print("DATA COLLECTION FINISHED  - {:02d}:{:02d}:{:02d} ----------------------------------------------"
              '\n'.format(utime.localtime()[3], utime.localtime()[4], utime.localtime()[5]), end='')

    def clear_arrays(self):
        '''@brief      reinitializes each array for data collection
        '''
        
        self.time_array = array.array('d', [])
        self.X_array = array.array('d', [])
        self.Y_array = array.array('d', [])
        self.X_vel_array = array.array('d', [])
        self.Y_vel_array = array.array('d', [])
        self.Theta_x_array = array.array('d', [])
        self.Theta_y_array = array.array('d', [])
        self.Omega_x_array = array.array('d', [])
        self.Omega_y_array = array.array('d', [])
        self.PWM1_array = array.array('f', [])
        self.PWM2_array = array.array('f', [])

    def clear_queues(self):
        '''@brief       empties the queues for next data collection
           @details     queues sometimes have remainder values, so this function
                        is needed to make sure queue is cleared for next data collection
        '''
        
        while self.touch_queue.num_in():
            self.touch_queue.get()
        while self.imu_queue.num_in():
            self.imu_queue.get()
        while self.time_duty_queue.num_in():
            self.time_duty_queue.get()

    def run(self):
        '''@brief   runs task user
           @details constantly waiting for correct input from user to execute
       '''

        if utime.ticks_us() >= self.next_time:

            self.next_time = utime.ticks_us() + self.period

            '''@brief   if user time still running the rest will execute
            '''

            if self.state == 0:
                '''@brief   state 0 gives instructions then proceeds to state 1
                '''

                print("+---------------------------------------------------------------------------+\n"
                      "| Balancing Ball Final Project - Matt/Philip                                |\n"
                      "+---------------------------------------------------------------------------+\n"
                      "| Use the following commands:                                               |\n"
                      "|     b     Start Balancing                                                 |\n"
                      "|     a     Stop Balancing                                                  |\n"
                      "|     g     Start collecting touch panel and IMU data                       |\n"
                      "|     s     Stop collecting touch panel and IMU data                        |\n"
                      "|     h     Display this help message                                       |\n"
                      "| Ctrl + C  End program                                                     |\n"
                      "+---------------------------------------------------------------------------+\n"
                      )

                self.state = 1

            elif self.state == 1:

                '''@brief   waits for 6 commands listed above
                   @param   one of 6 inputs
                   @details if any of 6 commands is hit then appropriate command will follow
                '''

                if self.collect.read():

                    while self.touch_queue.num_in() > 0:
                        # MAY need to separate queue while loop because different sizes??
                        '''@brief       appends all positional and angular values to tuples
                        '''
                        
                        touch = self.touch_queue.get()
                        imu = self.imu_queue.get()
                        time_duty = self.time_duty_queue.get()

                        self.time_array.append(time_duty[0])
                        self.X_array.append(touch[0])
                        self.Y_array.append(touch[1])
                        self.X_vel_array.append(touch[2])
                        self.Y_vel_array.append(touch[3])
                        self.Theta_x_array.append(imu[0])
                        self.Theta_y_array.append(imu[1])
                        self.Omega_x_array.append(imu[2])
                        self.Omega_y_array.append(imu[3])
                        self.PWM1_array.append(time_duty[1])
                        self.PWM2_array.append(time_duty[2])

                elif self.stop_data.read():
                    self.collect.write(0)
                    self.stop_data.write(0)
                    self.print_data()

                # waiting for user input state
                if (self.port.any()):

                    user_input = self.port.read(1).decode()

                    if user_input == ('a'):
                        '''@brief   'a' stops board balancing
                        '''
                        
                        self.balancing.write(0)
                        print("BALANCING has stopped")

                    elif user_input == ('b'):
                        '''@brief   'b' commences board balancing
                        '''
                        
                        # self.balancing.write(1)
                        # TEST CALCULATION SIGNS and FUNCTIONALITY
                        self.balancing.write(1)
                        print("BALANCING has begun")

                    elif self.collect.read() == 0 and user_input == ('g'):
                        '''@brief   'g' starts collecting data
                        '''
                        
                        print(
                            'COLLECTING BALANCING DATA - {:02d}:{:02d}:{:02d} ----------------------------------------------'
                            '\n'.format(utime.localtime()[3], utime.localtime()[4], utime.localtime()[5]), end='')
                        self.collect.write(1)
                        self.clear_queues()
                        self.clear_arrays()
                        self.startTime.write(utime.ticks_us())

                    elif self.collect.read() == 1 and user_input == ('s'):
                        '''@brief   stops data collection
                        '''
                        
                        self.collect.write(0)
                        self.print_data()

                    elif user_input == ('h'):
                        '''@brief   calls up help menu listing user commands
                        '''
                        
                        self.state = 0

            # elif (self.state == 2):
            #     if (self.port.any()):
            #         user_input = self.port.read(1).decode()
            #         if user_input == ('c'):
            #             print("FAULT CLEARED\n")
            #             self.clr_fault.write(1)
            #             self.state = 1
            #             self.step1.write(0)
            #             self.step2.write(0)
            #             self.duty1.write(0)
            #             self.duty2.write(0)
            #             self.stop1.write(1)
            #             self.stop2.write(1)
