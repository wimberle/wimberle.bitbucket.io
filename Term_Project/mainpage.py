''' @file                       mainpage.py
    @author                     Philip Pang
    @author                     Matthew Wimberley
    @brief                      This develops the main page of our bitbucket portfolio
    @details                    Using doxywizard we are able to compile our entire website
                                using special documentation formatting our repository.
                                
    @mainpage                   Mechatronics Repository
    @page                       Intro Files
    
    @section sec_intro          Introduction
                                This website encapsulates all our hard work that went into
                                producing our ball balancing board. All source code for our 
                                python files can be found here:
                                https://bitbucket.org/wimberle/me305_labs/src/master/
                                    
    @section sec_main           Labs
                                Many of our files are revised heavily throughout the course
                                of this quarter, so links provided to the source code on 
                                bitbucket are provided.
                                
    @section sec_lab            Lab Descriptions:
        
                     			Lab 0 Fibonacci:
                     			This program outputs the Fibonacci number for whatever index input by the user; catch faults are in place.
                     
                        		Lab 1 LED:
                     			This program utilizes a nucleo board to shine a light at different brightnesses to test our ability to set different
                                pins on our microcontroller. 
                     
                        		Lab 2 Encoder:
                     			This lab uses a main encoder file and an encoder.py file that respectively calls the encoder file, and defines all the pins 
                                needed to receive data from the encoder. The encoder quickly reaches overflow, so a 'delta' variable must be continuously updated.
                     
                     			Lab 3 Motor:
                     			This lab tests our ability to command a DC motor with our STM32 microcontroller. We wish to control the motors' duty cycles 
                                while simultaneously tracking the position and angular velocity of the motor with our task_encoder.py file from the previous lab.
                     
                       			Lab 4 Controller:
                     			Creating a controller for our motor loop was necessary as the motor needs some fine tuning. We created a proportional only controller
                                for our motor to get better response times from our motor duty cycles.
                     
                        		Lab 5 Inertial Measurement Unit:
                     			This program makes use of the BNO055 Intertial Measurement Unit used to track the absolute orientation of our ball-board system.
                     
                     			Final Project Ball Balancing Platform:
                                All the previous labs this quarter cumulated into our final term project where our objective is to balance a ball 
                                on a board adjusting our motor duty cycles based off of matrix manipulation with the ball position and gains we set.
                                
    @page                       page 3 HW0x02
                                These are our calculations executed by hand to model the dynamics of
                                our ball board system.
                                
    @section HW0x23             Ball Balancer System Modeling
        @image                      html   ME305-Hw0x02-1.png   "HW2 Analysis Page 1"     width = 600px
        @image                      html   ME305-Hw0x02-2.png   "HW2 Analysis Page 2"     width = 600px
        @image                      html   ME305-Hw0x02-3.png   "HW2 Analysis Page 3"     width = 600px
        @image                      html   ME305-Hw0x02-4.png   "HW2 Analysis Page 4"     width = 600px
        @image                      html   ME305-Hw0x02-5.png   "HW2 Analysis Page 5"     width = 600px
        
    @section Matlab Simulink  Simulation
        @image                      html   ME305_HW3_Final-01.png   "HW3 Matlab Code Page 1"  width = 1000px
        @image                      html   ME305_HW3_Final-02.png   "HW3 Matlab Code Page 2"  width = 1000px
        @image                      html   ME305_HW3_Final-03.png   "HW3 Matlab Code Page 3"  width = 1000px
        @image                      html   ME305_HW3_Final-04.png   "HW3 Matlab Code Page 4"  width = 1000px
        @image                      html   ME305_HW3_Final-05.png   "HW3 Matlab Code Page 5"  width = 1000px
        @image                      html   ME305_HW3_Final-06.png   "HW3 Matlab Code Page 6"  width = 1000px
        @image                      html   ME305_HW3_Final-07.png   "HW3 Matlab Code Page 7"  width = 1000px
        @image                      html   ME305_HW3_Final-08.png   "HW3 Matlab Code Page 8"  width = 1000px
        @image                      html   ME305_HW3_Final-09.png   "HW3 Matlab Code Page 9"  width = 1000px
        @image                      html   ME305_HW3_Final-10.png   "HW3 Matlab Code Page 10"  width = 1000px
        @image                      html   ME305_HW3_Final-11.png   "HW3 Matlab Code Page 11"  width = 1000px
        @image                      html   ME305_HW3_Final-12.png   "HW3 Matlab Code Page 12"  width = 1000px
        @image                      html   ME305_HW3_Final-13.png   "HW3 Matlab Code Page 13"  width = 1000px
        @image                      html   ME305_HW3_Final-14.png   "HW3 Matlab Code Page 14"  width = 1000px
        @image                      html   ME305_HW3_Final-15.png   "HW3 Matlab Code Page 15"  width = 1000px
        @image                      html   ME305_HW3_Final-16.png   "HW3 Matlab Code Page 16"  width = 1000px
        @image                      html   ME305_HW3_Final-17.png   "HW3 Matlab Code Page 17"  width = 1000px
        @image                      html   ME305_HW3_Final-18.png   "HW3 Matlab Code Page 18"  width = 1000px
        @image                      html   ME305_HW3_Final-19.png   "HW3 Matlab Code Page 19"  width = 1000px
        
    @page Lab0
    @section sec_lab0               Lab0 Fibonacci
                         			This program outputs the Fibonacci number for whatever index input by the user; catch faults are in place.
 			
                                    Lab0 Documentation can be found here:
    *       ME305_Lab0.py 




    @page Lab1
    @section sec_lab1               Lab 1 LED 
                         			This program utilizes a nucleo board to shine a light at different brightnesses to test our ability to set different
                                    pins on our microcontroller. Documentation for the files needed for Lab 1 can be found in this file: 
    @image                          html   Lab1_FSM.png   "Lab 1 FSM Diagram"  width = 1000px \
    @htmlonly                       <iframe width="560" height="315" src="https://www.youtube.com/embed/HNlr2bozauA" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    @endhtmlonly
   *        ME305_Lab1_Pang_Wimberley.py
   *        encoder.py




    @page Lab2
    @section sec_lab2               Lab 2 Encoder Driver
                                    This lab uses a main encoder file and an encoder.py file that respectively calls the encoder 
                                    file, and defines all the pins needed to receive data from the encoder. The encoder quickly 
                                    reaches overflow, so a 'delta' variable must be continuously updated.
                                    Documentation and the files needed for Lab 2 can be found in these files: 
    @image                          html   program_diagrams-1.png    "Task Diagram"             width = 1000px
    @image                          html   program_diagrams-2.png    "State Machine Diagrams"   width = 1000px
   *       main.py 
   *	   task_user.py 
   *	   encoder.py
   *	   task_encoder.py
   *       shares.py




    @page Lab3
    @section sec_lab3               Lab 3 Motor Driver and User Interface
                         			This lab tests our ability to command a DC motor with our STM32 microcontroller. We wish to control 
                                    the motors' duty cycles while simultaneously tracking the position and angular velocity of the motor 
                                    with our task_encoder.py file from the previous lab.
    @image                          html   ME305-Lab4-diagrams-1.png    "Block and Task Diagrams"   width = 600px
    @image                          html   ME305-Lab4-diagrams-2.png    "State Machine Diagrams"    width = 600px
    @image                          html   me305-lab3-plots.png         "Open Loop Motor Plots"     width = 600px
    @brief                          Documentation and the files needed for Lab 3 can be found in these files: 
   *       task_safety.py 
   *	   task_user.py
   *       task_motor.py
   *	   encoder.py
   *	   task_encoder.py
   *	   DRV8847.py
   *	   encoder.py
   *       shares.py
   
   
   
   
    @page Lab4
    @section sec_lab4               Lab 4 Closed Loop Controller
                                    Creating a controller for our motor loop was necessary as the motor needs some fine tuning. We created a proportional only controller
                                    for our motor to get better response times from our motor duty cycles.
                                    Documentation and the files needed for Lab 4 can be found in these files: 
   *       main.py 
   *	   ClosedLoop.py 
   *	   DRV8847.py
   *	   task_encoder.py
   *	   task_motor.py
   *       task_user.py
   *	   DRV8847.py
   *       task_controller.py
   *	   shares.py
   *       encoder.py
  
    @section ControlLoop Results    Test Results
                                    Based on our results tuning our controller gains led to 
                                    improvements in the closed loop velocity output of the motor.
                                    In each of our plots with the closed loop control in place,
                                    the motor reaches steady state significantly faster. We were able to get to our target velocity
                                    within a half second for each Kp value we tested for. Additionally, the decay envelope of our
                                    transient response reached a minimum at around Kp = 0.35. This greatly reduced our response time.
     @image                         html   ME305-Lab4-diagrams-1.png    "Block and Task Diagrams"   width = 600px
     @image                         html   ME305-Lab4-diagrams-2.png    "State Machine Diagrams"    width = 600px
     @image                         html   ME305-Lab4-step_responses.png "Step Responses"           width = 900px




    @page Lab5
    @section sec_lab5               Lab 5 Inertial Measurement Unit Driver
                                    This program makes use of the BNO055 Intertial Measurement Unit used to track the absolute 
                                    orientation of our ball-board system.
                                    Documentation of files for Lab 5 can be found in these files: 
   *        BNO055.py


    @page Term_Project                     
    @section Final                  Deliverables
                                    These sections illustrate the capabilities and logic behind our ball balancing machine. 
                                    

   *        task_controller.py
   *        task_user.py
   *        DRV8847.py
   *        Touch_Panel.py
   *        task_touch.py
   *        task_imu.py
   *        shares.py
   *        main.py
   *        BNO055.py
   
   @section Touch_Panel             Touch Panel Test Results
                                    Our testing procedure went very smoothly. We first ran our finger on the touch panel
                                    to find the positive direction for the panel. Knowing the direction, we now know which 
                                    way to offset the coordinates so that (0,0) reads the center of the touch panel. Now
                                    that the center is found, we are able to receive values in a coordinate grid. We then 
                                    ran our code in a loop calculating the time to run after every time we touched the panel.
                                    
                                    Our first run through took around 900 microseconds. We then moved the uticks_diff 
                                    calculation outside of our print statement saving us around 30 micro seconds. In the end,
                                    we are consistently able to read our coordinates in about 830 microseconds.
                    
     @image                         html   ME305-Term-Project_Benchmark.png     "Touch Panel Benchmark"     width = 900px
     @image                         html   IMG_3135.png                         "Hardware Setup"            width = 900px
     
    @section Ball_Balancing_Demo    System Performance Video Demonstration
        @htmlonly                   <iframe width="560" height="315" src="https://www.youtube.com/embed/6oAg1jgSYJg" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        @endhtmlonly
        
    @section FSM_Diagrams           Term Project Task and State Diagrams
        @image                      html    ME305_TermProject_Diagrams-1.png    "Term Project FSM Diagrams"     width = 1000px
        @image                      html    ME305_TermProject_Diagrams-2.png    "Term Project Task Diagram"     width = 1000px
        @details                    The inner workings of our lab files have changed tremendously throughout the course of this term.
                                    Most significantly we removed a lot of redundacies in our final term project like task_encoder and task_motor
                                    as these are not necessary to control the position of our ball. Our task_controller handles the data compilation
                                    and is the intermediate for all our shares and queues. 
        
    @section TP_Plots               Balancing Data Plots
        @image                      html    ME-305_Term_Project_Plot.png        "Plots of Ball Kinematics, Board Angular Displacement, and Motor Output\n"    width = 1000px
        @details                    
        
'''
