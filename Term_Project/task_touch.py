''' @file           task_touch.py
    @brief          sets up touch panel data to be written for data collection
    @details        finds ball's velocity by subtracting from last position
                    and writing the new state
                    Source code for this file can be found here:
                    https://bitbucket.org/wimberle/me305_labs/src/master/305_Term_Project/task_touch.py

    @author         Philip Pang
    @author         Matthew Wimberley
    @date           October 26, 2021
'''

import utime

class TouchTasks:
    '''@brief       instantiates self object of Touch Panel tasks'''

    prev_state = (0,0,0)

    def __init__(self, freq_io, Touch_Panel, balancing, collect, touch_state):

        '''@brief   handles same input parameters as task_imu.py
           '''

        self.freq_io = freq_io
        '''@brief   touch screen frequency
        '''
        
        self.period = 1 / freq_io * 1000000  # in us (microseconds)
        '''@brief   touch screen period in microseconds
        '''
        
        self.next_time = 0
        '''@brief   sets next time to establish velocity
        '''

        self.TouchPanel = Touch_Panel
        '''@brief   passes touch panel object
        '''

        self.balancing = balancing
        '''@brief   passes balancing object
        '''
        
        self.collect = collect
        '''@brief   passes collect object/flag
        '''
        
        self.touch_state = touch_state
        '''@brief   passes touch screen status
        '''

    def run(self):
        '''@brief       runs ball position and velocity tracker from touch panel data   
           @details     writes touch screen data at our desired period
           @param       must call object of itself
           '''
           
        if (utime.ticks_us() >= self.next_time):

            self.next_time = utime.ticks_us() + self.period

            ball = self.TouchPanel.scan_all()
            velocity = (0,0)

            if not ball[2]:
                ball = (0,0, False)         # if ball is not in contact, read ball as being at equilibrium
                self.prev_state = (0,0,0)   # if ball is not in contact, zero previous prev_state so alg will know exactly when it comes into contact
                # velocity = (0, 0)         # if ball is not in contact, zero velocity
            else:
                if self.prev_state == (0,0,0):      # when ball comes INTO contact for the first time
                    self.prev_state = ball
                    # velocity = (0, 0)             # if ball is not in contact, zero velocity
                else:                               # when ball is IN contact and HAS BEEN
                    velocity = (((ball[0]-self.prev_state[0])*self.freq_io ), ((ball[1]-self.prev_state[1])*self.freq_io))
                    self.prev_state = ball

            self.touch_state.write((ball[0], ball[1], velocity[0], velocity[1], ball[2])) # (x_scan, y_scan, x_vel, y_vel, z_scan)

