'''@file            ClosedLoop.py
   @brief           controls the output speed of the motor 
   @details         uses a PID controller to reduce decay time and increase time response
                    Source code found here: https://bitbucket.org/wimberle/me305_labs/src/master/305_Lab4/ClosedLoop.py    
   @author          Philip Pang
   @author          Matthew Wimberley
   @date            October 26, 2021      
'''   


class ClosedLoop:
    
    '''@brief       creates closed loop control on motor speed
       @details     P-controller multiples error by gain to optimize speed
    '''

    integral = 0        # stored because it needs to accumulate
    prev_error = 0      # stored because algorithm needs to remember from previous iteration

    def __init__(self, max_lim, min_lim):
        '''@brief   intialized proportional gain and saturation limits on motor
           @param   maximum limit
           @param   minimum limit
        '''

        self.max_lim = max_lim
        self.min_lim = min_lim

        
    def update(self, alg, target, curr_speed, kp, ki, kd):
        '''@brief   compute actuation value and update it
           @details Target: motor speed, current encoder position, duty cycle
           @param   PID gains
           @param   target and current speeds
           @return  returns error multiplied with gains
        '''

        # kp = 1/4000

        error = target - curr_speed
        self.integral += error
        derivative = error - self.prev_error
        effort = error * kp

        if alg == 2 or alg == 4: # CASE alg == PI or PID
            effort += self.integral * ki
        if alg == 3 or alg == 4: # CASE alg == PD or PID
            effort += derivative * kd

        self.prev_error = error

        if effort > self.max_lim:
            effort = self.max_lim
        elif effort < self.min_lim:
            effort = self.min_lim
        return effort

    def reset(self):
        '''@brief   resets gain values
        '''
        
        self.prev_error = 0
        self.integral = 0

    def set_Kp(self, kp):
        '''@brief   sets a new Kp value
        '''
        self.kp = kp

    def set_Ki(self, ki):
        '''@brief   sets a new Ki value
        '''
        
        self.ki = ki

    def set_Kd(self, kd):
        '''@brief   sets a new Kd value
        '''
        
        self.kd = kd

    def get_Kp(self):
        '''@brief   retrieves the new Kp values
           @return  Kp
        '''
        
        return self.kp

    def get_Ki(self):
        '''@brief   retrieves the new Kp values
           @return  Ki
        '''
        
        return self.ki

    def get_Kd(self):
        '''@brief   retrieves the new Kp values
           @return  Kd
        '''
        
        return self.kd

    # def tuning(self):
    #     '''@brief   let's us check if motor output matches encoder values
    #     '''
    #     print("Encoder: {}, Motor: {}".format(self.current_position, self.duty))
        