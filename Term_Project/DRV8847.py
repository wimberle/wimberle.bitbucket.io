'''@file           DRV8847.py
   @brief          controls motor driver
   @details        instantiates motor variables and motor commands
                    source code: https://bitbucket.org/wimberle/me305_labs/src/master/305_Lab3/DRV8847.py
                    Most recent version for our task project can be found here:
                    https://bitbucket.org/wimberle/me305_labs/src/master/305_Term_Project/BNO055.py
   
   @author         Philip Pang
   @author         Matthew Wimberley
   @date           November 1, 2021
      
'''

import pyb

# class DRV8847:
#
#
#     ''' @brief      A motor driver class for the DRV8847 from TI.
#         @details    Objects of this class can be used to configure the DRV8847
#                     motor driver and to create one or more objects of the
#                     Motor class which can be used to perform motor
#                     control. Refer to the DRV8847 datasheet here:
#                     https://www.ti.com/lit/ds/symlink/drv8847.pdf
#     '''
#
#
#     def __init__(self, timer, nSLEEP, nFAULT, fault_flag):
#
#         ''' @brief  Initializes and returns a DRV8847 object.
#             @param  timer and fault flags
#         '''
#         self.timer = pyb.Timer(timer, freq = 20000)
#         self.nSLEEP = pyb.Pin(nSLEEP , pyb.Pin.OUT_PP)
#         self.fault_int = pyb.ExtInt(nFAULT, mode = pyb.ExtInt.IRQ_FALLING,
#                            pull = pyb.Pin.PULL_NONE, callback = self.fault_cb)
#         self.fault_flag = fault_flag
#
#
#     def enable(self):
#
#
#         ''' @brief  Brings the DRV8847 out of sleep mode.
#         '''
#
#         self.fault_int.disable()                # Disable fault interrupt
#         self.nSLEEP.high()                      # Re-enable the motor driver
#
#         utime.sleep_us(25)                      # Wait for the fault pin to return high
#         self.fault_int.enable()                 # Re-enable the fault interrupt
#
#
#     def disable(self):
#
#         ''' @brief  Puts the DRV8847 in sleep mode.
#         '''
#
#         self.nSLEEP.low()
#
#
#     def fault_cb(self, IRQ_src):
#
#
#         ''' @brief  Callback function to run on fault condition.
#             @param  IRQ_src The source of the interrupt request.
#         '''
#         self.disable()
#         self.fault_flag.write(1)
#
#
#     def motor(self, mot):
#
#
#         ''' @brief  Initializes and returns a motor object associated with the DRV8847.
#             @return An object of class Motor
#         '''
#         if mot == 1:
#             return Motor(self.timer, 1, 2, pyb.Pin(pyb.Pin.cpu.B4), pyb.Pin(pyb.Pin.cpu.B5))
#         elif mot == 2:
#             return Motor(self.timer, 3, 4, pyb.Pin(pyb.Pin.cpu.B0), pyb.Pin(pyb.Pin.cpu.B1))


class Motor:


    ''' @brief      A motor class for one channel of the DRV8847.
        @details    Objects of this class can be used to apply PWM to a given
                    DC motor.
    '''


    def __init__(self, timer_no, ch1, ch2, pin1, pin2):

        
        ''' @brief      Initializes and returns a motor object associated with the DRV8847.
            @details    Objects of this class should not be instantiated
                        directly. Instead create a DRV8847 object and use
                        that to create Motor objects using the method
                        DRV8847.motor().
        '''
        self.timer = pyb.Timer(timer_no, freq=20000)
        self.in1 = self.timer.channel(ch1, pyb.Timer.PWM, pin = pin1)
        self.in2 = self.timer.channel(ch2, pyb.Timer.PWM, pin = pin2)


    def set_duty(self, duty):


        ''' @brief      Set the PWM duty cycle for the motor channel.
            @details    This method sets the duty cycle to be sent
                        to the motor to the given level. Positive values
                        cause effort in one direction, negative values
                        in the opposite direction.
            @param      duty A signed number holding the duty
                        cycle of the PWM signal sent to the motor
        '''
        # if greater than 100

        # if positive, spin CW
        if duty > 0:
            if duty > 100:
                self.in1.pulse_width_percent(100) 
            else:
                self.in1.pulse_width_percent(duty) 
            self.in2.pulse_width_percent(0)
        # if positive, spin CCW
        elif duty < 0:
            self.in1.pulse_width_percent(0) 
            if duty < -100:
                self.in2.pulse_width_percent(100)
            else:
                self.in2.pulse_width_percent(-duty)
        # if zero, don't spin
        else:
            self.in1.pulse_width_percent(0) 
            self.in2.pulse_width_percent(0) 


if __name__ == '__main__':
    '''@brief       pin objects instantiated
    '''
    
    # Adjust the following code to write a test program for your motor class. Any
    # code within the if __name__ == '__main__' block will only run when the
    # script is executed as a standalone program. If the script is imported as
    # a module the code block will not run.

    # Create a motor driver object and two motor objects. You will need to
    # modify the code to facilitate passing in the pins and timer objects needed
    # to run the motors.


    # fault_flag = shares.Share(0)
    # motor_drv = DRV8847(3, pyb.Pin(pyb.Pin.cpu.A15), pyb.Pin(pyb.Pin.cpu.B2),fault_flag)
    #
    # motor_1 = motor_drv.motor(1)
    # motor_2 = motor_drv.motor(2)

    motor_1 = Motor(3, 1, 2, pyb.Pin(pyb.Pin.cpu.B4), pyb.Pin(pyb.Pin.cpu.B5))
    motor_2 = Motor(3, 3, 4, pyb.Pin(pyb.Pin.cpu.B0), pyb.Pin(pyb.Pin.cpu.B1))

    # Enable the motor driver

    # Set the duty cycle of the first motor to 40 percent and the duty cycle of
    # the second motor to 60 percent
    
    # print("Motor 1 to 50")
    # motor_1.set_duty(50)
    print("Motor 2 to 50")
    motor_2.set_duty(50)
    print("hi")