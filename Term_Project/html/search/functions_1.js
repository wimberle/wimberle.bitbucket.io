var searchData=
[
  ['calib_5fstatus_0',['calib_status',['../classBNO055_1_1BNO055.html#aca9a22e2d1ca740e7a25d4346a9d4fad',1,'BNO055::BNO055']]],
  ['calibrate_1',['calibrate',['../classBNO055_1_1BNO055.html#aac7e7af1ab189be6d48906a063fa249d',1,'BNO055.BNO055.calibrate()'],['../classTouch__Panel_1_1Touch__Panel.html#af0b6dbb3b34f3222a4675acb8df50c1a',1,'Touch_Panel.Touch_Panel.calibrate()']]],
  ['change_5fmode_2',['change_mode',['../classBNO055_1_1BNO055.html#a46879e92a286997c935930164e6d850d',1,'BNO055::BNO055']]],
  ['clear_5farrays_3',['clear_arrays',['../classtask__user_1_1UserTasks.html#a20d57027076c5a2cf3b6a67d84e36d39',1,'task_user::UserTasks']]],
  ['clear_5fqueues_4',['clear_queues',['../classtask__user_1_1UserTasks.html#af11e7dc5bb89aed5a2673b7c1539c1ca',1,'task_user::UserTasks']]],
  ['collect_5fdata_5',['collect_data',['../classtask__controller_1_1ControllerTasks.html#aff14f77d25e02eb69f3399fce981a655',1,'task_controller::ControllerTasks']]]
];
