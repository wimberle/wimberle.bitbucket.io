'''@file                task_motor.py
   @brief               enables motors if no fault is occuring and disables faulted motor
   @details             constantly updating and checking for fault flag
                        Source code found here: https://bitbucket.org/wimberle/me305_labs/src/master/305_Lab3/task_motor.py  
                    
   @author              Philip Pang
   @author              Matthew Wimberley
   @date                October 30, 2021   
'''

import utime

class MotorTasks:
    def __init__(self, freq, motor, duty, fault_flag):
        
        '''@brief       initializes all motor task info
        
           @param       motor number 
           @param       motor's frequency
           @param       duty cycle
           @param       flag on/off
           '''
           
        self.freq = freq
        self.duty = duty
        self.motor = motor
        self.fault_flag = fault_flag
        self.period = 1 / freq * 1000000  # in us (microseconds)
        self.next_time = 0

    def run(self):
        
        '''@brief       as long as time continues forward, position and encoder data is retrieved
        
           @details     uses read() and write() methods to retrieve encoder data (firmware side)
           @param       must call object of itself
        '''
        if (utime.ticks_us() >= self.next_time):
            self.next_time = utime.ticks_us() + self.period
            
            if(self.fault_flag.read()):
                self.duty.write(0)
            self.motor.set_duty(self.duty.read()) # could change the actual self.duty share.Shares
            


