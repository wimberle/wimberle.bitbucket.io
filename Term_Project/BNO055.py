''' @file           BNO055.py
    @brief          sets up IMU the absolute orientation sensor
    @details        Using built in I2C commands and config modes, we must set up the 
                    calibration for the inertial measurement system to read. If no 
                    file is read, then a calibration file will be written to be saved
                    on the STM microncontroller on the board.
                    Source code for this file can be found here:
                    https://bitbucket.org/wimberle/me305_labs/src/master/305_Term_Project/BNO055.py
            


    @author         Philip Pang
    @author         Matthew Wimberley
    @date           December 5, 2021
'''




import utime
from pyb import I2C
import os

class BNO055:
    ''' @brief      sets up the IMU class
        @details    After the units, configuration mode, and calibration status
                    have been initialized, the IMU will collect the calibration coefficients
                    to record the absolute orientation and positioning of the system.
    '''

    def __init__(self, i2c):
        ''' @brief  initializes coefficients, angles, velocities, and status
            @details The length of each byte array is known and established off 
                    the motor's owner manual
        '''
        
        self.i2c = i2c
        '''@brief   initializes inter-integrated circuit
        '''
        
        self.coeff = bytearray(22)
        '''@brief   initializes coefficient array
        '''
        
        self.angles = bytearray(6)
        '''@brief   initializes angle array
        '''
        
        self.vels = bytearray(6)
        '''@brief   initializes velocity array
           @details same size as angle array as it's the derivative
        '''
        
        self.status = bytearray(1)
        '''@brief   initializes status array
           @details for calibration status
        '''

    def set_units(self, units):
        '''@brief   sets units for data
           @details units to be used to set up radians for angular measurements
                    and m/s^2 for g acceleration
        '''
                    
        mode = self.i2c.mem_read(1, 40, 0x3D)
        '''@brief   sets up correct mode, 0x28 in hexadecimal
        '''
        
        self.i2c.mem_write(0, 40, 0x3D)
        '''@brief   same as calibration mode
        '''
        
        self.i2c.mem_write(units, 40, 0x3B)
        '''@brief   calibration mode 59
        '''
        
        self.i2c.mem_write(mode, 40, 0x3D)
        '''@brief   calibration mode 61
        '''
        
    def change_mode(self, mode):
        '''@brief   to change mode back to 9 axis orientation mode
        '''
        
        self.i2c.mem_write(mode, 40, 0x3D)

    def calib_status(self):
        '''@brief   switches to calibration status mode
           @return  calibration status
        '''
        
        self.i2c.mem_read(self.status, 40, 0x35)
        return self.status[0]

    def read_coeff(self):
        '''@brief   reads coefficients just read prior
           @return  the calibration coefficients
        '''
        
        self.i2c.mem_read(self.coeff, 40, 0x55)
        return self.coeff

    def write_coeff(self, coefficients):
        '''@brief   writes coefficients to IMU
        '''
        
        self.i2c.mem_write(coefficients, 40, 0x55)

    def read_angles(self):
        '''@brief   reads angles using first calibration mode
           @return  angles in radians
        '''
        
        self.i2c.mem_read(self.angles, 40, 0x1A)
        '''@brief   orientation of IMU with respect to Earth
        '''
        
        ang = [0,0,0]
        ang[0] = self.angles[0] | self.angles[1] << 8  # EUL_Heading
        ang[1] = self.angles[2] | self.angles[3] << 8  # EUL_Roll
        ang[2] = self.angles[4] | self.angles[5] << 8  # EUL_Pitch
        for i in range(3):
            if ang[i] > 32767:
                ang[i] -= 65536
        # print('Euler Angles - (rad)\n'
        #       'Heading:{:}\tRoll:{:}\tPitch:{:}'.format(ang[0]/900, ang[1]/900, ang[2]/900))
        return (ang[0]/900, ang[1]/900, ang[2]/900)

    def read_velocity(self):
        '''@brief   read velocity using mode 20 set up in BNO055
           @return  velocities in radians per second
        '''
        
        self.i2c.mem_read(self.vels, 40, 0x14)
        '''@brief   back to mode 20 for accelerometer
        '''
        
        vel = [0,0,0]
        vel[0] = self.vels[0] | self.vels[1] << 8  # EUL_Roll
        vel[1] = self.vels[2] | self.vels[3] << 8  # EUL_Pitch
        vel[2] = self.vels[4] | self.vels[5] << 8  # EUL_Heading
        for i in range(3):
            if vel[i] > 32767:
                vel[i] -= 65536
        # print('Angular Velocities - (rad/s)\n'
        #       'X:{:}\tY:{:}\tZ:{:}'.format(vel[0]/900, vel[1]/900, vel[2]/900))
        return (vel[0]/900, vel[1]/900, vel[2]/900)

    def print_calib(self):
        '''@brief   prints out calibration status
           @details This is extremely useful for calibration as this tells us
                    exactly which components still need to be calibrated based 
                    on the output integer. 255 means a fully calibrated IMU
           @return  calibration status
        '''
        
        out = [False, False, False, False]
        SYS = 0b11000000
        GYR = 0b00110000
        ACC = 0b00001100
        MAG = 0b00000011
        if (self.status[0] & SYS) == SYS:
            out[0] = True
        if (self.status[0] & GYR) == GYR:
            out[1] = True
        if (self.status[0] & ACC) == ACC:
            out[2] = True
        if (self.status[0] & MAG) == MAG:
            out[3] = True
        return out

    def calibrate(self):
        '''@brief   sets up calibration file to be written and read by IMU
           @details If no file is read, then the the unit will write the coefficients 
                    to be saved in an external text file. This file can be read
                    for the next calibration, so we do not have to go through that 
                    hassle again.
           @return  calibration coefficients text file
        '''
           
        print("CALIBRATING IMU...")

        filename = "IMU_cal_coeffs.txt"

        self.change_mode(12)
        self.set_units(6)

        try:
            with open(filename, 'r') as f:
                cal_data_string = f.readline()
                # coefficients should be bytearray that is 22 bytes long
                arr = cal_data_string.strip().split(",")
                arrayOfBytes = bytearray(22)
                for i in range(22):
                    arrayOfBytes[i] = int(arr[i], 16)
                self.write_coeff(arrayOfBytes)

        except:
            print("Shake machine, tilt board, etc...")
            print("[SYS,  GYR,  ACC,  MAG]")
            next_time = utime.ticks_us() + 1000000
            while True:
                # print(str(self.print_calib()) + "\t" + str(binary))
                if(utime.ticks_us() >= next_time):
                    print(str(self.print_calib()) + "\t" + str(self.calib_status())) #self.imu_state.read()
                    next_time = utime.ticks_us() + 1000000
                if (self.calib_status() == 0b11111111):
                    self.write_coeff(self.read_coeff())
                    cal_data_string = ""

                    for coeff in self.coeff:
                        cal_data_string += hex(coeff) + ", "

                    cal_data_string = cal_data_string[:-2]

                    with open(filename, 'w') as f:
                        f.write(f"{cal_data_string}\r\n")
                    break

        print("IMU CALIBRATION COMPLETE!")

if __name__ == '__main__':

    i2c = I2C(1, I2C.MASTER)
    imu = BNO055(i2c)

    imu.calibrate()

    while True:
        print(imu.read_angles())
        print(imu.read_velocity())
        utime.sleep(1)