''' @file           task_controller.py
    @brief          instantiates all controller variables to optimize PID;
    @details        handles unit conversion of ticks and controls speed error.
                    Also sets up gains to be used explicitly my controller system.
                    This file handles all the math for the motor input needed to 
                    center the ball.

    @author         Philip Pang
    @author         Matthew Wimberley
    @date           November 4, 2021
'''


import utime
import DRV8847
from ulab import numpy as np

class ControllerTasks:
    '''@brief       instantiates motor, balancing, and collection objects
       @details     this utilizes the same objects passed as task_user
    '''

    duty1 = 0
    duty2 = 0
    freq_collect = 20 # Hz
    
    def __init__(self, freq_io, Motor1, Motor2, touch_state, imu_state, balancing, collect, stop_data,
                                                  startTime, touch_queue, imu_queue, time_duty_queue):
        
        ''' @brief      instantiates gains, target speed, and flag
            @details    period is as small as we can receive data. Same parameters discussed before.
                        We remove a lot of objects from previous labs as there are not pertinent 
                        for the full operation of our ball balancing board and only slow down the 
                        processing time for all tasks. This way our controller runs as efficiently 
                        as possible.
        '''
        
        self.freq_io = freq_io
        self.period = 1 / freq_io * 1000000    # in us (microseconds)
        self.next_time = 0
        
        
        self.period_collect = 1 / self.freq_collect * 1000000
        self.next_collect = 0

        self.motor1 = Motor1
        self.motor2 = Motor2

        self.touch_state = touch_state
        self.imu_state = imu_state

        self.balancing = balancing
        self.collect = collect
        self.stop_data = stop_data
        self.startTime = startTime

        self.touch_queue = touch_queue
        self.imu_queue = imu_queue
        self.time_duty_queue = time_duty_queue

        self.elapsed_time = 0



    def run(self):
                
        ''' @brief      runs closed control loop
            @details    converts units to radians and updates all gain and speed values
                        Does not return any values but prints out motor duty cycles only with 
                        angular position and angular velocity of the board/motor system.
                        These K1 and K2 gains have been fine tuned to rotate quickly but 
                        not overshoot the board. 
        '''
        
        if (utime.ticks_us() >= self.next_time):
            self.next_time = utime.ticks_us() + self.period
            
            if self.balancing.read():
                self.elapsed_time = utime.ticks_diff(utime.ticks_us(), self.startTime.read())

                touch = self.touch_state.read()     # (x_scan, y_scan, x_vel, y_vel, z_scan)
                imu = self.imu_state.read()         # (theta_y [roll], theta_x [pitch], omega_y, omega_x)

                state_1 = np.array([[touch[0]/1000], [imu[1]], [touch[2]/1000], [imu[2]]])  # (x, theta_y [roll], x_dot, omega_y)
                state_2 = np.array([[touch[1]/1000], [imu[0]], [touch[3]/1000], [imu[3]]])  # (y, theta_x [pitch], y_dot, omega_x)
                
                K1_gains = np.array([4.5, -1.9, .3, -0.12])     # [N, N*m, N*s, N*m*s]
                # K1_gains = np.array([15, -.9, .8, -.08])   # [N, N*m, N*s, N*m*s]

                K2_gains = np.array([-4.5, -1.9, -.3, 0.12])    # [N, N*m, N*s, N*m*s]
                # K2_gains = np.array([-15, -.9, -.8, .08])   # [N, N*m, N*s, N*m*s]
                
                # states could go other way around and would need to be switched
                self.duty1 = np.dot(K1_gains, state_1)[0]*100*2.21/(4*0.0138*12)
                self.duty2 = np.dot(K2_gains, state_2)[0]*100*2.21/(4*0.0138*12)

                #-----------------------
                self.motor1.set_duty(self.duty1)    # next to touch panel's LONG side
                self.motor2.set_duty(self.duty2)    # next to touch panel's SHORT side
                print(f"X:{touch[0]} Y:{touch[1]} X_dot:{touch[2]} Y_dot:{touch[3]}")
                print(f"theta-y:{imu[0]} theta-x:{imu[1]} omega-y:{imu[2]} omega-x:{imu[3]}")
                #-------------------------

                print(f"1:{self.duty1}, 2:{self.duty2}\n")
            else:
                self.motor1.set_duty(0)
                self.motor2.set_duty(0)

            if (utime.ticks_us() >= self.next_collect) and self.collect.read() and self.stop_data.read() == 0:
                self.next_collect = utime.ticks_us() + self.period_collect
                self.collect_data()

    def collect_data(self):
        '''@brief       touch screen, IMU, and duty cycle queues read here
           @details     This function collects all pertinent data for our ball-board system.
        ''' 
        
        self.touch_queue.put(self.touch_state.read())                                  # (x_scan, y_scan, x_vel, y_vel, z_scan)
        self.imu_queue.put(self.imu_state.read())                                      # (theta_y [roll], theta_x [pitch], omega_y, omega_x)
        self.time_duty_queue.put((self.elapsed_time, self.duty1, self.duty2))          # (time, duty1, duty2)
