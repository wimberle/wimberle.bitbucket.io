''' @file           task_encoder.py
    @brief          creates objects of all variables in main in task_encoder
    @details        sets up use for both encoders to relay informations
    
    @author         Philip Pang
    @author         Matthew Wimberley
    @date           October 26, 2021
'''

import utime
import array

class EncoderTasks:
    '''@brief       instantiates self object of encoder tasks'''

    def __init__(self, enc, freq, zero_pos, curr_pos, delta, duty, collect, stop, data_queue, startTime):

        '''@brief   initializes all encoder information
           @param   same as main.py
           '''

        self.zero_pos = zero_pos
        self.curr_pos = curr_pos
        self.delta = delta
        ### -------------------------
        self.duty = duty
        self.collect = collect
        self.stop = stop
        self.data_queue = data_queue
        self.startTime = startTime
        ### -------------------------
        self.freq = freq
        self.period = 1 / freq * 1000000  # in us (microseconds)
        self.next_time = 0
        self.enc = enc
        
        self.time_array1 = array.array('d',[])
        self.velocity_array1 = array.array('d',[])
        self.duty_array1 = array.array('d',[]) # added these 3 arrays to store values

    def run(self):
        # @brief   resets encoder positions then updates it
        if (utime.ticks_us() >= self.next_time):
            '''@brief       as long as time continues forward, position and encoder data is retrieved
               @details     uses read() and write() methods to retrieve encoder data (firmware side)
               @param       must call object of itself
               '''
            self.next_time = utime.ticks_us() + self.period
            
            if (self.zero_pos.read()):
                self.enc.set_position(0)
                self.zero_pos.write(False)
                
            if(self.collect.read()):
                self.collect_data()

            self.enc.update()

            self.delta.write(self.enc.get_delta())
            self.curr_pos.write(self.enc.get_position())
            
            
            
            #I moved collection from task_user to task_encoder below
            
        
    def collect_data(self):
        
            elapsed_time = utime.ticks_diff(utime.ticks_us(), self.startTime.read())
            if elapsed_time >= 30000000:
                self.collect.write(0)
                self.stop.write(1)

                
            else:
                data_tuple = (elapsed_time, self.delta.read(), self.duty.read())
                self.data_queue.put(data_tuple)
    
